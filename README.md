# CHALLENGE: REPORTING TRENDS FOR SPATIAL PROCESSES 

<img src="https://github.com/unepwcmc/unbl-cloudnativehacks/assets/141819111/36de6e41-b173-4c1d-a9a2-f1f17fcf1d74" height="100"/>

**SDG 15 Life on Land**

Forest losses, land degradation, and species extinctions are escalating. These events pose severe threats to the survival of people and the planet. The Global Biodiversity Framework of the UN Biodiversity Convention provides renewed impetus for conserving terrestrial ecosystems, in line with SDG15. 
  
The UN Biodiversity Lab (UNBL) is a web-based tool that supports countries, NGOs, and civil society in accessing relevant geospatial datasets to provide valuable insights about their territories, facilitating strategic decision making, and smart reporting. Currently, UNBL is being further developed to align with the needs of countries in relation to the Global Biodiversity Framework to support spatial planning, monitoring and reporting needs. We invite you to build an attractive dashboard and/or widgets based on cloud native datasets to offer more advanced analysis. 

In this document, we will publish some relevant datasets and give you some ideas to explore. We are still working on it. Stay tuned.
